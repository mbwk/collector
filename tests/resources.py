SAMPLE_IP_GEOLOCATION = {
    "city": "Minneapolis",
    "country": {"iso_code": "US", "name": "United States"},
    "location": {
        "accuracy_radius": 20,
        "latitude": 45.04,
        "longitude": -93.4865,
        "metro_code": 613,
        "time_zone": "America/Chicago",
    },
}
