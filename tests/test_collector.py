from datetime import datetime
from unittest import (
    mock,
    TestCase,
)

from starlette.testclient import TestClient

from .resources import SAMPLE_IP_GEOLOCATION

mock.patch(
    "collector.geoip_service.geolocate_ip", return_value=SAMPLE_IP_GEOLOCATION
).start()

from collector.main import app, SessionLocal
from collector.crud import _delete_events, _events_count, query_events


client = TestClient(app)


class EventSubmissionTestCase(TestCase):
    def setUp(self):
        self.db = SessionLocal()
        self.assertEqual(_events_count(self.db), 0)
        pass

    def tearDown(self):
        _delete_events(self.db)

    def test_submit_event_normal(self):
        response = client.post(
            "/submit",
            json={
                "source_ip": "128.101.101.101",
                "name": "Test Event",
                "timestamp": datetime(2020, 2, 6).isoformat(),
            },
        )

        self.assertTrue(response.ok, "Failed to accept normal post request")
        self.assertEqual(response.json()["message"], "Event submitted")

    def test_submit_event_invalid(self):
        response = client.post(
            "/submit",
            json={
                "source_ip": "not a valid IP address",
                # "name": "Test Event",  # Missing `name`
                "timestamp": "not a valid timestamp",
            },
        )

        self.assertFalse(response.ok, "Invalid input was accepted")

    def test_event_created_with_geolocation(self):
        client.post(
            "/submit",
            json={
                "source_ip": "128.101.101.101",
                "name": "Test Event",
                "timestamp": datetime(2020, 2, 6).isoformat(),
            },
        )
        events = query_events(self.db)
        event, geolocation = events[0]
        self.assertEqual(event.geolocation_id, geolocation.geolocation_id)

    def test_query_events_by_timerange(self):
        dates = [
            datetime(2019, 2, 7),
            datetime(2020, 1, 12),
            datetime(2020, 1, 20),
            datetime(2020, 2, 6),
            datetime(2020, 2, 17),
            datetime(2020, 3, 1),
            datetime(2020, 5, 2),
        ]
        for dt in dates:
            client.post(
                "/submit",
                json={
                    "source_ip": "128.101.101.101",
                    "name": "Test Event",
                    "timestamp": dt.isoformat(),
                },
            )

        response = client.get(
            "/fetch",
            params={
                "timerange_from": datetime(2020, 2, 1),
                "timerange_until": datetime(2020, 3, 1),
            },
        )
        self.assertTrue(response.ok)

        resjson = response.json()
        self.assertIsInstance(resjson, list)
        self.assertEqual(len(resjson), 2)

    def test_query_events_by_city(self):
        client.post(
            "/submit",
            json={
                "source_ip": "128.101.101.101",
                "name": "Test Event",
                "timestamp": datetime(2020, 2, 6).isoformat(),
            },
        )

        good_query = client.get("/fetch", params={"city": "Minneapolis"})
        self.assertTrue(good_query.ok)
        self.assertEqual(len(good_query.json()), 1)

        bad_query = client.get("/fetch", params={"city": "Seattle"})
        self.assertTrue(bad_query.ok)
        self.assertEqual(len(bad_query.json()), 0)

    def test_query_events_by_country(self):
        client.post(
            "/submit",
            json={
                "source_ip": "128.101.101.101",
                "name": "Test Event",
                "timestamp": datetime(2020, 2, 6).isoformat(),
            },
        )

        good_query = client.get("/fetch", params={"country": "US"})
        self.assertTrue(good_query.ok)
        self.assertEqual(len(good_query.json()), 1)

        bad_query = client.get("/fetch", params={"country": "DE"})
        self.assertTrue(bad_query.ok)
        self.assertEqual(len(bad_query.json()), 0)

    def test_query_events_combined_query(self):
        dates = [
            datetime(2019, 2, 7),
            datetime(2020, 1, 12),
            datetime(2020, 1, 20),
            datetime(2020, 2, 6),
            datetime(2020, 2, 17),
            datetime(2020, 3, 1),
            datetime(2020, 5, 2),
        ]
        for dt in dates:
            client.post(
                "/submit",
                json={
                    "source_ip": "128.101.101.101",
                    "name": "Test Event",
                    "timestamp": dt.isoformat(),
                },
            )

        good_query = client.get(
            "/fetch",
            params={
                "city": "Minneapolis",
                "country": "US",
                "timerange_from": datetime(2020, 2, 1),
                "timerange_until": datetime(2020, 3, 1),
            },
        )
        self.assertTrue(good_query.ok)

        good_json = good_query.json()
        self.assertEqual(len(good_json), 2)

        bad_query = client.get(
            "/fetch",
            params={
                "city": "Frankfurt",
                "country": "DE",
                "timerange_from": datetime(2020, 2, 1),
                "timerange_until": datetime(2020, 3, 1),
            },
        )
        self.assertTrue(bad_query.ok)

        bad_json = bad_query.json()
        self.assertEqual(len(bad_json), 0)
