#!/usr/bin/env bash

set -e

docker run --rm \
    -it $ARGS \
    -v $(pwd):/srv/collector \
    collector_api \
    $@

