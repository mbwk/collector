#!/usr/bin/env bash

set -e

DOCKER_RUN="$( dirname "${BASE_SOURCE[0]}" )/bin/dockerize.sh"

ARGS="--env-file=envs/testing.env" $DOCKER_RUN pytest tests/

