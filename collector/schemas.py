from datetime import datetime
from ipaddress import (
    IPv4Address,
    IPv6Address,
)
from typing import (
    Any,
    Dict,
    Optional,
    Union,
)


from pydantic import BaseModel


class GeolocationBase(BaseModel):
    city: str
    country_iso: str
    metro_code: int
    time_zone: str

    accuracy_radius: int

    latitude: float
    longitude: float


class Geolocation(GeolocationBase):
    geolocation_id: int

    class Config:
        orm_mode = True


class GeolocationCreate(GeolocationBase):
    pass


class EventBase(BaseModel):
    timestamp: datetime
    source_ip: Union[IPv4Address, IPv6Address]
    name: str
    detail: Optional[Dict[str, Any]]


class EventCreate(EventBase):
    pass


class Event(EventBase):
    event_id: int
    geolocation_id: Optional[int]

    class Config:
        orm_mode = True


class EventGeolocated(EventBase):
    geolocation: GeolocationBase

    class Config:
        orm_mode = True
