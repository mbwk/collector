from datetime import datetime
from typing import (
    List,
    Optional,
)


from fastapi import (
    BackgroundTasks,
    Depends,
    FastAPI,
)
from sqlalchemy.orm import Session


from . import (
    __version__,
    models,
    schemas,
)
from .crud import (
    save_event,
    save_geolocation_data,
    query_events,
)
from .database import (
    SessionLocal,
    engine,
)
from .geoip_service import geolocate_ip

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


async def geolocate_and_store(db_event: models.Event):
    ip_str = db_event.source_ip

    geolocate_data = geolocate_ip(ip_str)
    loc = geolocate_data["location"]

    db = SessionLocal()
    save_geolocation_data(
        db,
        db_event.event_id,
        schemas.GeolocationCreate(
            city=geolocate_data["city"],
            country_iso=geolocate_data["country"]["iso_code"],
            metro_code=loc["metro_code"],
            accuracy_radius=loc["accuracy_radius"],
            latitude=loc["latitude"],
            longitude=loc["longitude"],
            time_zone=loc["time_zone"],
        ),
    )


@app.get("/")
async def read_root():
    return "collector api v{}".format(__version__)


@app.post("/submit")
async def submit_event(
    event: schemas.EventCreate,
    background_tasks: BackgroundTasks,
    db: Session = Depends(get_db),
):
    db_event = save_event(db, event)
    background_tasks.add_task(geolocate_and_store, db_event)
    return {"message": "Event submitted"}


@app.get("/fetch", response_model=List[schemas.EventGeolocated])
async def fetch_events(
    city: Optional[str] = None,
    country: Optional[str] = None,
    timerange_from: Optional[datetime] = None,
    timerange_until: Optional[datetime] = None,
    db: Session = Depends(get_db),
):
    results = query_events(db, city, country, timerange_from, timerange_until)

    events = [
        schemas.EventGeolocated(
            timestamp=db_event.timestamp,
            source_ip=db_event.source_ip,
            name=db_event.name,
            detail=db_event.detail,
            geolocation=schemas.GeolocationBase(
                city=db_geolocation.city,
                country_iso=db_geolocation.country_iso,
                metro_code=db_geolocation.metro_code,
                time_zone=db_geolocation.time_zone,
                accuracy_radius=db_geolocation.accuracy_radius,
                latitude=db_geolocation.latitude,
                longitude=db_geolocation.longitude,
            ).dict(),
        )
        for db_event, db_geolocation in results
    ]

    return events
