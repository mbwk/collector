import os
from typing import Any


DEFAULTS = {
    "MAX_TRIES": 8,
}


class ConfigurationError(Exception):
    pass


def getconfig(config_key: str) -> Any:
    config_value = os.getenv(config_key) or DEFAULTS.get(config_key)
    if config_value is None:
        raise ConfigurationError
    return config_value
