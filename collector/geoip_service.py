from backoff import on_exception, expo
from ratelimit import limits, RateLimitException
import requests


from .settings import getconfig


class ServiceError(Exception):
    pass


class ServiceBadRequestError(ServiceError):
    pass


class ServiceUpstreamError(ServiceError):
    pass


GEOIP_API_URL = getconfig("GEOIP_API_URL")


@on_exception(expo, RateLimitException, max_tries=getconfig("MAX_TRIES"))
@limits(calls=5, period=5)
def geolocate_ip(ip_str: str) -> dict:
    response = requests.get(GEOIP_API_URL, {"ip": ip_str})

    if not response.ok:
        if response.status_code == 400:
            raise ServiceBadRequestError(response.json())
        elif response.status_code == 429:
            raise RateLimitException
        else:
            raise ServiceUpstreamError(response.json())

    return dict(response.json())
