from sqlalchemy import (
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    JSON,
    String,
)
from sqlalchemy.orm import relationship


from .database import Base


class Geolocation(Base):
    __tablename__ = "geolocation"

    geolocation_id = Column(Integer, primary_key=True, index=True)
    ip_address = Column(String(50))

    city = Column(String(30))
    country_iso = Column(String(4))
    metro_code = Column(Integer)
    time_zone = Column(String(50))

    accuracy_radius = Column(Integer)

    latitude = Column(Float)
    longitude = Column(Float)

    event = relationship("Event", uselist=False, back_populates="geolocation")


class Event(Base):
    __tablename__ = "event"

    event_id = Column(Integer, primary_key=True, index=True)
    timestamp = Column(DateTime, index=True)
    source_ip = Column(String(50))
    name = Column(String)
    detail = Column(JSON)

    geolocation_id = Column(
        Integer,
        ForeignKey("geolocation.geolocation_id", ondelete="cascade"),
        nullable=True,
    )
    geolocation = relationship("Geolocation", back_populates="event", uselist=False)
