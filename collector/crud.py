from datetime import datetime
from typing import (
    List,
    Optional,
    Tuple,
)

from sqlalchemy.orm import Session

from . import models, schemas


def save_event(db: Session, event: schemas.EventCreate) -> models.Event:
    db_event = models.Event(
        name=event.name, source_ip=str(event.source_ip), timestamp=event.timestamp,
    )
    db.add(db_event)
    db.commit()
    db.refresh(db_event)
    return db_event


def get_event(db: Session, event_id: int) -> models.Event:
    return db.query(models.Event).filter(models.Event.event_id == event_id).first()


def query_events(
    db: Session,
    city: Optional[str] = None,
    country: Optional[str] = None,
    timerange_from: Optional[datetime] = None,
    timerange_until: Optional[datetime] = None,
) -> List[Tuple[models.Event, models.Geolocation]]:
    query_object = db.query(models.Event, models.Geolocation,).filter(
        models.Geolocation.geolocation_id == models.Event.geolocation_id
    )

    if city:
        query_object = query_object.filter(models.Geolocation.city == city)
    if country:
        query_object = query_object.filter(models.Geolocation.country_iso == country)
    if timerange_from:
        query_object = query_object.filter(models.Event.timestamp > timerange_from)
    if timerange_until:
        query_object = query_object.filter(models.Event.timestamp < timerange_until)

    return query_object.all()


def _delete_events(db: Session):
    db.query(models.Event).delete()
    db.commit()


def _events_count(db: Session) -> int:
    return len(db.query(models.Event).all())


def save_geolocation_data(
    db: Session, event_id: int, geolocation: schemas.GeolocationCreate
):
    db_event = get_event(db, event_id)

    db_geolocation = models.Geolocation(**geolocation.dict())
    db.add(db_geolocation)
    db.commit()
    db.refresh(db_geolocation)
    db_event.geolocation_id = db_geolocation.geolocation_id
    db.commit()
