FROM python:3.8-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /srv/collector/
RUN apt -y update \
    && apt -y install curl less wget jq vim build-essential libpq-dev \
    && apt -y clean \
    && apt -y autoremove \
    && rm -rf /var/lib/apt/lists/*
    
ENV PATH="$PATH:/root/.poetry/bin"

RUN pip install --upgrade pip \
    && curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/1.0.3/get-poetry.py | python

COPY poetry.lock pyproject.toml /srv/collector/

RUN poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi

EXPOSE 8000 8000

CMD [ "gunicorn", "collector.main:app", "-b", ":8000", \
        "-w", "2", "-k", "uvicorn.workers.UvicornWorker" ]

COPY . /srv/collector/

